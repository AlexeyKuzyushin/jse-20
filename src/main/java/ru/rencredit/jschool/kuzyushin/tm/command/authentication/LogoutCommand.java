package ru.rencredit.jschool.kuzyushin.tm.command.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;

public final class LogoutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Logout";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        if (serviceLocator != null) {
            serviceLocator.getAuthService().logout();
        }
        System.out.println("[OK]");
    }
}
