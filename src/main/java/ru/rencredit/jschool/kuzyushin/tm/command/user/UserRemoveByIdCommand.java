package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class UserRemoveByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-remove-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user by id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        if (serviceLocator != null) {
            serviceLocator.getUserService().removeById(id);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
