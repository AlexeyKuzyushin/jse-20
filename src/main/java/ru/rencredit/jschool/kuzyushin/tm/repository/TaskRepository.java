package ru.rencredit.jschool.kuzyushin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.ITaskRepository;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void add(final @NotNull String userId,
                    final @NotNull Task task) {
        task.setUserId(userId);
        records.add(task);
    }

    @Override
    public void remove(final @NotNull String userId, final @NotNull Task task) {
        if (!userId.equals(task.getUserId())) return;
        this.records.remove(task);
    }

    @NotNull
    @Override
    public List<Task> findAll(final @NotNull String userId) {
        final @NotNull List<Task > result = new ArrayList<>();
        for (final @NotNull Task task: records) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(final @NotNull String userId) {
        List<Task> tasks = findAll(userId);
        this.records.removeAll(tasks);
    }

    @Nullable
    @Override
    public Task findOneById(final @NotNull String userId,
                            final @NotNull String id) {
        final @NotNull List<Task> tasks = findAll(userId);
        for (final @NotNull Task task: tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Nullable
    @Override
    public Task findOneByName(final @NotNull String userId,
                              final @NotNull String name) {
        final @NotNull List<Task> tasks = findAll(userId);
        for (final @NotNull Task task: tasks) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Nullable
    @Override
    public Task findOneByIndex(final @NotNull String userId,
                               final @NotNull Integer index) {
        final List<Task> tasks = findAll(userId);
        if (!tasks.isEmpty()) return tasks.get(index);
        return null;
    }

    @Nullable
    @Override
    public Task removeByName(final @NotNull String userId,
                             final @NotNull String name) {
        final @Nullable Task task = findOneByName(userId, name);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Nullable
    @Override
    public Task removeByIndex(final @NotNull String userId,
                              final @NotNull Integer index) {
        final @Nullable Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Nullable
    @Override
    public Task removeById(final @NotNull String userId,
                           final @NotNull String id) {
        final @Nullable Task task = findOneById(userId, id);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }
}