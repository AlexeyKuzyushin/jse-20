package ru.rencredit.jschool.kuzyushin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;

public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        if (serviceLocator != null) {
            @NotNull final String userId = serviceLocator.getAuthService().getUserId();
            serviceLocator.getProjectService().clear(userId);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
