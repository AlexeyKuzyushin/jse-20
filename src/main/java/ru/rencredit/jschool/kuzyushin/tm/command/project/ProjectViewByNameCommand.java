package ru.rencredit.jschool.kuzyushin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class ProjectViewByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-view-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by name";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        if (serviceLocator != null) {
            @NotNull final String userId = serviceLocator.getAuthService().getUserId();
            @Nullable final String name = TerminalUtil.nextLine();
            @Nullable final Project project = serviceLocator.getProjectService().findOneByName(userId, name);
            if (project == null) return;
            System.out.println("ID: " + project.getId());
            System.out.println("NAME: " + project.getName());
            System.out.println("DESCRIPTION: " + project.getDescription());
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
