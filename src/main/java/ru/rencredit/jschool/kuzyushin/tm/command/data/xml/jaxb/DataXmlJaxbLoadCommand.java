package ru.rencredit.jschool.kuzyushin.tm.command.data.xml.jaxb;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.constant.DataConstant;
import ru.rencredit.jschool.kuzyushin.tm.dto.Domain;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataXmlJaxbLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-xml-jaxb-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from xml file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML LOAD]");
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");

        @NotNull final File file = new File(DataConstant.FILE_XML_JAXB);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        if (serviceLocator != null) {
            serviceLocator.getDomainService().load(domain);
        }
        else {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}