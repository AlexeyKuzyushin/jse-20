package ru.rencredit.jschool.kuzyushin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER ID:");
        @NotNull final String userId;
        if (serviceLocator != null) {
            userId = serviceLocator.getAuthService().getUserId();
            @Nullable final String id = TerminalUtil.nextLine();
            @Nullable final Task task = serviceLocator.getTaskService().removeOneById(userId, id);
            if (task != null) System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
