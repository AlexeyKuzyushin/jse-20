package ru.rencredit.jschool.kuzyushin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-update-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by index";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER INDEX:");

        if (serviceLocator != null) {
            @NotNull final String userId = serviceLocator.getAuthService().getUserId();
            @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
            @Nullable final Project project = serviceLocator.getProjectService().findOneByIndex(userId, index);

            System.out.println("ENTER NAME:");
            @Nullable final String name = TerminalUtil.nextLine();
            System.out.println("ENTER DESCRIPTION:");
            @Nullable final String description = TerminalUtil.nextLine();
            @Nullable final Project projectUpdated = serviceLocator.getProjectService().
                    updateOneByIndex(userId, index, name, description);
            if (projectUpdated != null) System.out.println("[OK]");
            else System.out.println("[FAILED]");
        }
        else System.out.println("[FAILED]");
    }
}
